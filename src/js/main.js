import template from './main.html';
import * as test from './components/test';

angular
	.module('app')
	.component('mainComponent', {
		template: template
});