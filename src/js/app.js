// styles
import styles from '../scss/app.scss';

// javascript
import angular from 'angular';

angular
  .module('app', []);

require('./main.js');
