import template from './test.html';

angular
  .module('app')
  .component('test', {
    template: template,
    controller: TestController
  });

function TestController() {
  const vm = this;

  vm.$onInit = function() {
    console.log('test');
  }
}
